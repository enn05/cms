

import React, { Component, PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  MenuItem,
  DropdownButton,
  Panel, PageHeader, ListGroup, ListGroupItem, Button,
} from 'react-bootstrap';


import s from './Home.css';
import StatWidget from '../../components/Widget';
import Donut from '../../components/Donut';

import {
  Tooltip,
  XAxis, YAxis, Area,
  CartesianGrid, AreaChart, Bar, BarChart,
  ResponsiveContainer
} from '../../vendor/recharts';

const title = 'Sb Admin React';


// const data = [
//       { name: 'Page A', uv: 4000, pv: 2400, amt: 2400, value: 600 },
//       { name: 'Page B', uv: 3000, pv: 1398, amt: 2210, value: 300 },
//       { name: 'Page C', uv: 2000, pv: 9800, amt: 2290, value: 500 },
//       { name: 'Page D', uv: 2780, pv: 3908, amt: 2000, value: 400 },
//       { name: 'Page E', uv: 1890, pv: 4800, amt: 2181, value: 200 },
//       { name: 'Page F', uv: 2390, pv: 3800, amt: 2500, value: 700 },
//       { name: 'Page G', uv: 3490, pv: 4300, amt: 2100, value: 100 },
// ];

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      timeLine: [],
      isLoading: true,
      showAll: true,
      showConfirmed: false,
      showRecovered: false,
      showDeaths: false,
    }
  }

  componentDidMount() {
    fetch('https://corona-api.com/countries/PH')
      .then(res => res.json())
      .then(data => {
        // console.log('DATA',data.data.timeline)
        const timeLine = []
        const casesPerDay = data.data.timeline
        casesPerDay.map(indivCase => {
          console.log(indivCase)
          let perDay = {
            date: indivCase.date,
            deaths: indivCase.new_deaths,
            recovered: indivCase.new_recovered,
            confirmed: indivCase.new_confirmed
          }
          timeLine.unshift(perDay)
        })
        this.setState({
          data: data.data.latest_data,
          timeLine: timeLine,
          isLoading: false
        })
      })
    console.log('states', this.state)
  }

  handleShowAll() {
    console.log('all')
    this.setState({
      showAll: true,
      showConfirmed: false,
      showDeaths: false,
      showRecovered: false
    })
  }

  handleShowConfirmed() {
    console.log('confirmed')
    this.setState({
      showAll: false,
      showConfirmed: true,
      showDeaths: false,
      showRecovered: false
    })
  }

  handleShowRecovered() {
    console.log('recovered')
    this.setState({
      showAll: false,
      showConfirmed: false,
      showDeaths: false,
      showRecovered: true
    })
  }

  handleShowDeaths() {
    console.log('deaths')
    this.setState({
      showAll: false,
      showConfirmed: false,
      showDeaths: true,
      showRecovered: false
    })
  }

  render() {
    return (
      <div style={{}}>
        <div className="row">
          <div className="col-lg-12" style={{ marginTop: '50px' }}>
            <PageHeader>Dashboard</PageHeader>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-3 col-md-6">
            <StatWidget
              isLoading={this.state.isLoading}
              style="panel-primary panel-shadow"
              // icon="fa fa-comments fa-5x"
              count={`${this.state.data.confirmed}`}
              headerText="Confirmed Cases"
              footerText="View Details"
              linkTo="/"
            />
          </div>
          <div className="col-lg-3 col-md-6">
            <StatWidget
              isLoading={this.state.isLoading}
              style="panel-green panel-shadow"
              // icon="fa fa-tasks fa-5x"
              count={`${this.state.data.recovered}`}
              headerText="Recovered"
              footerText="View Details"
              linkTo="/"
            />
          </div>
          <div className="col-lg-3 col-md-6">
            <StatWidget
              isLoading={this.state.isLoading}
              style="panel-yellow panel-shadow"
              // icon="fa fa-shopping-cart fa-5x"
              count={`${this.state.data.critical}`}
              headerText="Critical"
              footerText="View Details"
              linkTo="/"
            />
          </div>
          <div className="col-lg-3 col-md-6">
            <StatWidget
              isLoading={this.state.isLoading}
              style="panel-red panel-shadow"
              // icon="fa fa-support fa-5x"
              count={`${this.state.data.deaths}`}
              headerText="Deaths"
              footerText="View Details"
              linkTo="/"
            />
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">

            <Panel
              className="panel-shadow"
              header={<span>
                <i className="fa fa-bar-chart-o fa-fw" /> Cases Per Day
              <div className="pull-right">
                  <DropdownButton title={this.state.showAll ? "All" : this.state.showConfirmed ? "Confirmed" : this.state.showRecovered ? "Recovered" : this.state.showDeaths ? "Deaths" : "Select"} bsSize="xs" pullRight id="dropdownButton1" >
                    <MenuItem eventKey="1" onClick={() => this.handleShowAll()}>All</MenuItem>
                    <MenuItem eventKey="2" onClick={() => this.handleShowConfirmed()}>Confirmed</MenuItem>
                    <MenuItem eventKey="3" onClick={() => this.handleShowRecovered()}>Recovered</MenuItem>
                    <MenuItem eventKey="4" onClick={() => this.handleShowDeaths()}>Deaths</MenuItem>
                  </DropdownButton>
                </div>
              </span>}
            >
              {this.state.timeLine.length <= 0 ?
                <div className="d-flex justify-content-center align-items-center text-center" style={{ height: '40vh', padding: 'auto' }}>
                  <div>
                    <i
                      className="fa fa-refresh"
                    />&nbsp;Loading...
                </div>
                </div>
                :
                <div>
                  <ResponsiveContainer width="100%" aspect={2}>
                    <AreaChart data={this.state.timeLine} margin={{ top: 10, right: 30, left: 0, bottom: 0 }} >
                      <XAxis dataKey="date" />
                      <YAxis />
                      <CartesianGrid stroke="#ccc" />
                      <Tooltip />
                      {this.state.showDeaths || this.state.showAll ?
                        <Area type="monotone" dataKey="deaths" stackId="1" stroke="#d9534f" fill="#d9534f" />
                        : ""
                      }
                      {this.state.showRecovered || this.state.showAll ?
                        <Area type="monotone" dataKey="recovered" stackId="1" stroke="#5cb85c" fill="#5cb85c" />
                        : ""
                      }
                      {this.state.showConfirmed || this.state.showAll ?
                        <Area type="monotone" dataKey="confirmed" stackId="1" stroke="#337ab7" fill="#337ab7" />
                        : ""
                      }
                    </AreaChart>
                  </ResponsiveContainer>
                </div>
              }

            </Panel>

            {/* <Panel
            header={<span>
              <i className="fa fa-bar-chart-o fa-fw" /> Bar Chart Example
              <div className="pull-right">
                <DropdownButton title="Dropdown" bsSize="xs" pullRight id="dropdownButton2">
                  <MenuItem eventKey="1">Action</MenuItem>
                  <MenuItem eventKey="2">Another action</MenuItem>
                  <MenuItem eventKey="3">Something else here</MenuItem>
                  <MenuItem divider />
                  <MenuItem eventKey="4">Separated link</MenuItem>
                </DropdownButton>
              </div>
            </span>}
          >
            <div>
              <ResponsiveContainer width="100%" aspect={2}>
                <BarChart data={this.state.timeLine} margin={{ top: 10, right: 30, left: 0, bottom: 0 }} >
                  <CartesianGrid stroke="#ccc" />
                  <XAxis dataKey="date" />
                  <YAxis />
                  <Tooltip />
                  <Bar dataKey="recovered" stackId="1" fill="#8884d8" />
                  <Bar dataKey="confirmed" stackId="1" fill="#82ca9d" />
                  <Bar type="monotone" dataKey="deaths" fill="#ffc658" />
                </BarChart>
              </ResponsiveContainer>
            </div>
          </Panel> */}
          </div>

          <div className="col-lg-4">

            {/* <Panel
            header={<span>
              <i className="fa fa-bell fa-fw" /> Notifications Panel
            </span>}
          >
            <ListGroup>
              <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
                <i className="fa fa-comment fa-fw" /> New Comment
                <span className="pull-right text-muted small"><em>4 minutes ago</em></span>
              </ListGroupItem>
              <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
                <i className="fa fa-twitter fa-fw" /> 3 New Followers
                <span className="pull-right text-muted small"><em>12 minutes ago</em></span>
              </ListGroupItem>
              <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
                <i className="fa fa-envelope fa-fw" /> Message Sent
                <span className="pull-right text-muted small"><em>27 minutes ago</em></span>
              </ListGroupItem>
              <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
                <i className="fa fa-tasks fa-fw" /> New Task
                <span className="pull-right text-muted small"><em>43 minutes ago</em></span>
              </ListGroupItem>
              <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
                <i className="fa fa-upload fa-fw" /> Server Rebooted
                <span className="pull-right text-muted small"><em>11:32 AM</em></span>
              </ListGroupItem>
              <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
                <i className="fa fa-bolt fa-fw" /> Server Crashed!
                <span className="pull-right text-muted small"><em>11:13 AM</em></span>
              </ListGroupItem>
              <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
                <i className="fa fa-warning fa-fw" /> Server Not Responding
                <span className="pull-right text-muted small"><em>10:57 AM</em></span>
              </ListGroupItem>
              <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
                <i className="fa fa-shopping-cart fa-fw" /> New Order Placed
                <span className="pull-right text-muted small"><em>9:49 AM</em></span>
              </ListGroupItem>
              <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
                <i className="fa fa-money fa-fw" /> Payment Received
                <span className="pull-right text-muted small"><em>Yesterday</em></span>
              </ListGroupItem>
            </ListGroup>
            <Button block>View All Alerts</Button>
          </Panel> */}

            {/* <Panel
            header={<span>
              <i className="fa fa-bar-chart-o fa-fw" /> Donut Chart Example
            </span>}
          >
            <div>
              <Donut data={data} color="#8884d8" innerRadius="70%" outerRadius="90%" />
            </div>
          </Panel> */}

          </div>

        </div>
      </div>
    );
  }
}

// function Home(props, context) {
//   console.log(props)
//   context.setTitle(title);
//   return (
//     <div style={{ }}>
//       <div className="row">
//         <div className="col-lg-12">
//           <PageHeader>Dashboard</PageHeader>
//         </div>
//       </div>

//       <div className="row">
//         <div className="col-lg-3 col-md-6">
//           <StatWidget
//             style="panel-primary"
//             icon="fa fa-comments fa-5x"
//             count="26"
//             headerText="New Comments!"
//             footerText="View Details"
//             linkTo="/"
//           />
//         </div>
//         <div className="col-lg-3 col-md-6">
//           <StatWidget
//             style="panel-green"
//             icon="fa fa-tasks fa-5x"
//             count="12"
//             headerText="New Tasks!"
//             footerText="View Details"
//             linkTo="/"
//           />
//         </div>
//         <div className="col-lg-3 col-md-6">
//           <StatWidget
//             style="panel-yellow"
//             icon="fa fa-shopping-cart fa-5x"
//             count="124"
//             headerText="New Orders!"
//             footerText="View Details"
//             linkTo="/"
//           />
//         </div>
//         <div className="col-lg-3 col-md-6">
//           <StatWidget
//             style="panel-red"
//             icon="fa fa-support fa-5x"
//             count="13"
//             headerText="Support Tickets!"
//             footerText="View Details"
//             linkTo="/"
//           />
//         </div>
//       </div>

//       <div className="row">
//         <div className="col-lg-8">

//           <Panel
//             header={<span>
//               <i className="fa fa-bar-chart-o fa-fw" /> Area Chart Example
//               <div className="pull-right">
//                 <DropdownButton title="Dropdown" bsSize="xs" pullRight id="dropdownButton1" >
//                   <MenuItem eventKey="1">Action</MenuItem>
//                   <MenuItem eventKey="2">Another action</MenuItem>
//                   <MenuItem eventKey="3">Something else here</MenuItem>
//                   <MenuItem divider />
//                   <MenuItem eventKey="4">Separated link</MenuItem>
//                 </DropdownButton>
//               </div>
//             </span>}
//           >
//             <div>
//               <ResponsiveContainer width="100%" aspect={2}>
//                 <AreaChart data={data} margin={{ top: 10, right: 30, left: 0, bottom: 0 }} >
//                   <XAxis dataKey="name" />
//                   <YAxis />
//                   <CartesianGrid stroke="#ccc" />
//                   <Tooltip />
//                   <Area type="monotone" dataKey="uv" stackId="1" stroke="#8884d8" fill="#8884d8" />
//                   <Area type="monotone" dataKey="pv" stackId="1" stroke="#82ca9d" fill="#82ca9d" />
//                   <Area type="monotone" dataKey="amt" stackId="1" stroke="#ffc658" fill="#ffc658" />
//                 </AreaChart>
//               </ResponsiveContainer>
//             </div>

//           </Panel>

//           <Panel
//             header={<span>
//               <i className="fa fa-bar-chart-o fa-fw" /> Bar Chart Example
//               <div className="pull-right">
//                 <DropdownButton title="Dropdown" bsSize="xs" pullRight id="dropdownButton2">
//                   <MenuItem eventKey="1">Action</MenuItem>
//                   <MenuItem eventKey="2">Another action</MenuItem>
//                   <MenuItem eventKey="3">Something else here</MenuItem>
//                   <MenuItem divider />
//                   <MenuItem eventKey="4">Separated link</MenuItem>
//                 </DropdownButton>
//               </div>
//             </span>}
//           >
//             <div>
//               <ResponsiveContainer width="100%" aspect={2}>
//                 <BarChart data={data} margin={{ top: 10, right: 30, left: 0, bottom: 0 }} >
//                   <CartesianGrid stroke="#ccc" />
//                   <XAxis dataKey="name" />
//                   <YAxis />
//                   <Tooltip />
//                   <Bar dataKey="pv" stackId="1" fill="#8884d8" />
//                   <Bar dataKey="uv" stackId="1" fill="#82ca9d" />
//                   <Bar type="monotone" dataKey="amt" fill="#ffc658" />
//                 </BarChart>
//               </ResponsiveContainer>
//             </div>
//           </Panel>

//           {/* <Panel
//             header={<span>
//               <i className="fa fa-clock-o fa-fw" /> Responsive Timeline
//             </span>}
//           > */}
//             {/* <div>
//               <ul className="timeline">
//                 <li>
//                   <div className="timeline-badge"><i className="fa fa-check" />
//                   </div>
//                   <div className="timeline-panel">
//                     <div className="timeline-heading">
//                       <h4 className="timeline-title">Lorem ipsum dolor</h4>
//                       <p>
//                         <small className="text-muted">
//                           <i className="fa fa-clock-o" /> 11 hours ago via Twitter
//                         </small>
//                       </p>
//                     </div>
//                     <div className="timeline-body">
//                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero
//                         laboriosam dolor perspiciatis omnis exercitationem. Beatae, officia
//                         pariatur? Est cum veniam excepturi. Maiores praesentium, porro voluptas
//                         suscipit facere rem dicta, debitis.
//                       </p>
//                     </div>
//                   </div>
//                 </li>
//                 <li className="timeline-inverted">
//                   <div className="timeline-badge warning"><i className="fa fa-credit-card" />
//                   </div>
//                   <div className="timeline-panel">
//                     <div className="timeline-heading">
//                       <h4 className="timeline-title">Lorem ipsum dolor</h4>
//                     </div>
//                     <div className="timeline-body">
//                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem dolorem
//                         quibusdam, tenetur commodi provident cumque magni voluptatem libero, quis
//                         rerum. Fugiat esse debitis optio, tempore. Animi officiis alias, officia
//                         repellendus.
//                       </p>
//                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium
//                         maiores odit qui est tempora eos, nostrum provident explicabo dignissimos
//                         debitis vel! Adipisci eius voluptates, ad aut recusandae minus eaque facere.
//                       </p>
//                     </div>
//                   </div>
//                 </li>
//                 <li>
//                   <div className="timeline-badge danger"><i className="fa fa-bomb" />
//                   </div>
//                   <div className="timeline-panel">
//                     <div className="timeline-heading">
//                       <h4 className="timeline-title">Lorem ipsum dolor</h4>
//                     </div>
//                     <div className="timeline-body">
//                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus
//                         numquam facilis enim eaque, tenetur nam id qui vel velit similique nihil
//                         iure molestias aliquam, voluptatem totam quaerat, magni commodi quisquam.
//                       </p>
//                     </div>
//                   </div>
//                 </li>
//               </ul>
//             </div>
//           </Panel> */}

//         </div>

//         <div className="col-lg-4">

//           <Panel
//             header={<span>
//               <i className="fa fa-bell fa-fw" /> Notifications Panel
//             </span>}
//           >
//             <ListGroup>
//               <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
//                 <i className="fa fa-comment fa-fw" /> New Comment
//                 <span className="pull-right text-muted small"><em>4 minutes ago</em></span>
//               </ListGroupItem>
//               <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
//                 <i className="fa fa-twitter fa-fw" /> 3 New Followers
//                 <span className="pull-right text-muted small"><em>12 minutes ago</em></span>
//               </ListGroupItem>
//               <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
//                 <i className="fa fa-envelope fa-fw" /> Message Sent
//                 <span className="pull-right text-muted small"><em>27 minutes ago</em></span>
//               </ListGroupItem>
//               <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
//                 <i className="fa fa-tasks fa-fw" /> New Task
//                 <span className="pull-right text-muted small"><em>43 minutes ago</em></span>
//               </ListGroupItem>
//               <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
//                 <i className="fa fa-upload fa-fw" /> Server Rebooted
//                 <span className="pull-right text-muted small"><em>11:32 AM</em></span>
//               </ListGroupItem>
//               <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
//                 <i className="fa fa-bolt fa-fw" /> Server Crashed!
//                 <span className="pull-right text-muted small"><em>11:13 AM</em></span>
//               </ListGroupItem>
//               <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
//                 <i className="fa fa-warning fa-fw" /> Server Not Responding
//                 <span className="pull-right text-muted small"><em>10:57 AM</em></span>
//               </ListGroupItem>
//               <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
//                 <i className="fa fa-shopping-cart fa-fw" /> New Order Placed
//                 <span className="pull-right text-muted small"><em>9:49 AM</em></span>
//               </ListGroupItem>
//               <ListGroupItem href="" onClick={(e) => { e.preventDefault(); }}>
//                 <i className="fa fa-money fa-fw" /> Payment Received
//                 <span className="pull-right text-muted small"><em>Yesterday</em></span>
//               </ListGroupItem>
//             </ListGroup>
//             <Button block>View All Alerts</Button>
//           </Panel>

//           <Panel
//             header={<span>
//               <i className="fa fa-bar-chart-o fa-fw" /> Donut Chart Example
//             </span>}
//           >
//             <div>
//               <Donut data={data} color="#8884d8" innerRadius="70%" outerRadius="90%" />
//             </div>
//           </Panel>

//         </div>

//       </div>
//     </div>
//   );
// }

Home.propTypes = {
  // news: PropTypes.arrayOf(PropTypes.shape({
  //   title: PropTypes.string.isRequired,
  //   link: PropTypes.string.isRequired,
  //   contentSnippet: PropTypes.string,
  // })).isRequired,
};
Home.contextTypes = { setTitle: PropTypes.func.isRequired };

export default withStyles(s)(Home);
